# talktomyhand-frontend

[![pipeline status](https://gitlab.com/CosmicDarine/talktomyhand-frontend/badges/master/pipeline.svg)](https://gitlab.com/CosmicDarine/talktomyhand-frontend/-/commits/master)


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
