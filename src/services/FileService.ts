/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ApiMessageDTO } from '../models/ApiMessageDTO';
import type { FileCreationDTO } from '../models/FileCreationDTO';
import type { FileDTO } from '../models/FileDTO';
import { request as __request } from '../core/request';

export class FileService {

    /**
     * Get a specific sign's video.
     * This private endpoint is used to get a sign video.
     * @param signName 
     * @returns FileDTO Get file successful.
     * @throws ApiError
     */
    public static async getSignVideo(
signName: string,
): Promise<FileDTO> {
        const result = await __request({
            method: 'GET',
            path: `/files/video/${signName}`,
        });
        return result.body;
    }

    /**
     * Change a specific sign's video.
     * This private endpoint is used to upload a sign video.
     * @param requestBody 
     * @returns ApiMessageDTO Upload successful.
     * @throws ApiError
     */
    public static async uploadSignVideo(
requestBody?: FileCreationDTO,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'POST',
            path: `/files/video`,
            body: requestBody,
        });
        return result.body;
    }

}