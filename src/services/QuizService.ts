/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ApiMessageDTO } from '../models/ApiMessageDTO';
import type { QuizAddPollDTO } from '../models/QuizAddPollDTO';
import type { QuizCreationDTO } from '../models/QuizCreationDTO';
import type { QuizDTO } from '../models/QuizDTO';
import type { QuizModificationDTO } from '../models/QuizModificationDTO';
import { request as __request } from '../core/request';

export class QuizService {

    /**
     * Add polls to quiz.
     * This private endpoint is used to update a quiz in the database.
     * @param requestBody 
     * @returns ApiMessageDTO Poll in quiz updated successfully.
     * @throws ApiError
     */
    public static async addPollToQuiz(
requestBody?: QuizAddPollDTO,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/quizzes/addPoll`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Get the quiz.
     * This private endpoint is used to get the quiz with the given id in the database.
     * @param name 
     * @returns QuizDTO Get quiz successful.
     * @throws ApiError
     */
    public static async getQuiz(
name: string,
): Promise<QuizDTO> {
        const result = await __request({
            method: 'GET',
            path: `/quizzes/${name}`,
        });
        return result.body;
    }

    /**
     * update the quiz.
     * This private endpoint is used to update a quiz in the database.
     * @param name 
     * @param requestBody 
     * @returns QuizDTO Quiz updated successfully.
     * @throws ApiError
     */
    public static async updateQuiz(
name: string,
requestBody?: QuizModificationDTO,
): Promise<QuizDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/quizzes/${name}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Delete a specific quiz.
     * This private endpoint is used to remove one quiz.
     * @param name 
     * @returns ApiMessageDTO Quiz deleted successfully.
     * @throws ApiError
     */
    public static async deleteQuizByName(
name: string,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'DELETE',
            path: `/quizzes/${name}`,
        });
        return result.body;
    }

    /**
     * Get all quizzes.
     * This private endpoint is used to get all the quizzes.
     * @returns QuizDTO Get quizzes successful.
     * @throws ApiError
     */
    public static async getQuizzes(): Promise<Array<QuizDTO>> {
        const result = await __request({
            method: 'GET',
            path: `/quizzes`,
        });
        return result.body;
    }

    /**
     * Add the quiz.
     * This private endpoint is used to add a new quiz in the database.
     * @param requestBody 
     * @returns QuizDTO Quiz creation successful.
     * @throws ApiError
     */
    public static async createQuiz(
requestBody?: QuizCreationDTO,
): Promise<QuizDTO> {
        const result = await __request({
            method: 'POST',
            path: `/quizzes`,
            body: requestBody,
        });
        return result.body;
    }

}