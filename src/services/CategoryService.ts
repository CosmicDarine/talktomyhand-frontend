/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ApiMessageDTO } from '../models/ApiMessageDTO';
import type { CategoryAddSignDTO } from '../models/CategoryAddSignDTO';
import type { CategoryCreationDTO } from '../models/CategoryCreationDTO';
import type { CategoryDTO } from '../models/CategoryDTO';
import type { CategoryModificationDTO } from '../models/CategoryModificationDTO';
import { request as __request } from '../core/request';

export class CategoryService {

    /**
     * Add the sign to the category.
     * This private endpoint is used to update a category in the database.
     * @param requestBody 
     * @returns ApiMessageDTO Sign category updated successfully.
     * @throws ApiError
     */
    public static async addSignToCategory(
requestBody?: CategoryAddSignDTO,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/categories/addSign`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Get the category.
     * This private endpoint is used to get the category with the given id in the database.
     * @param name 
     * @returns CategoryDTO Get category successful.
     * @throws ApiError
     */
    public static async getCategory(
name: string,
): Promise<CategoryDTO> {
        const result = await __request({
            method: 'GET',
            path: `/categories/${name}`,
        });
        return result.body;
    }

    /**
     * Delete a specific category.
     * This private endpoint is used to remove one category.
     * @param name 
     * @returns ApiMessageDTO Category deleted successfully.
     * @throws ApiError
     */
    public static async deleteCategoryByName(
name: string,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'DELETE',
            path: `/categories/${name}`,
        });
        return result.body;
    }

    /**
     * Get all categories.
     * This private endpoint is used to get all the categories created by the user.
     * @returns CategoryDTO Get categories successful.
     * @throws ApiError
     */
    public static async getCategories(): Promise<Array<CategoryDTO>> {
        const result = await __request({
            method: 'GET',
            path: `/categories`,
        });
        return result.body;
    }

    /**
     * Add the category.
     * This private endpoint is used to add a new category in the database.
     * @param requestBody 
     * @returns CategoryDTO Category creation successful.
     * @throws ApiError
     */
    public static async createCategory(
requestBody?: CategoryCreationDTO,
): Promise<CategoryDTO> {
        const result = await __request({
            method: 'POST',
            path: `/categories`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Modify the category
     * This private endpoint is used to update a category in the database.
     * @param requestBody 
     * @returns ApiMessageDTO Category updated successfully.
     * @throws ApiError
     */
    public static async updateCategory(
requestBody?: CategoryModificationDTO,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/categories`,
            body: requestBody,
        });
        return result.body;
    }

}