/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ApiMessageDTO } from '../models/ApiMessageDTO';
import type { UserAddPointsForLessonDTO } from '../models/UserAddPointsForLessonDTO';
import type { UserDTO } from '../models/UserDTO';
import type { UserModificationDTO } from '../models/UserModificationDTO';
import { request as __request } from '../core/request';

export class UserService {

    /**
     * Get all users with minimal infos.
     * This private endpoint is used to get all users.
     * @returns UserDTO Get users successful.
     * @throws ApiError
     */
    public static async getUsersMinimal(): Promise<Array<UserDTO>> {
        const result = await __request({
            method: 'GET',
            path: `/users`,
        });
        return result.body;
    }

    /**
     * Get all users.
     * This private endpoint is used to get all users.
     * @returns UserDTO Get users successful.
     * @throws ApiError
     */
    public static async getUsers(): Promise<Array<UserDTO>> {
        const result = await __request({
            method: 'GET',
            path: `/admin/users`,
        });
        return result.body;
    }

    /**
     * Delete all users.
     * This private endpoint is used to remove all users.
     * @returns ApiMessageDTO Users deletion successful.
     * @throws ApiError
     */
    public static async deleteUsers(): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'DELETE',
            path: `/admin/users`,
        });
        return result.body;
    }

    /**
     * Update informations of a specific user.
     * This private admin endpoint is used to modify a specific user.
     * @param username 
     * @param requestBody 
     * @returns ApiMessageDTO User update successful.
     * @throws ApiError
     */
    public static async updateUserByAdmin(
username: string,
requestBody?: UserDTO,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/admin/users/${username}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Update informations of a specific user.
     * This private admin endpoint is used to modify a specific user.
     * @param requestBody 
     * @returns ApiMessageDTO User update successful.
     * @throws ApiError
     */
    public static async updateUserPoints(
requestBody?: UserAddPointsForLessonDTO,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/users/lesson`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Get a specific user.
     * This private endpoint is used to get one user.
     * @param username 
     * @returns UserDTO Get user successful.
     * @throws ApiError
     */
    public static async getUserByUsername(
username: string,
): Promise<UserDTO> {
        const result = await __request({
            method: 'GET',
            path: `/users/${username}`,
        });
        return result.body;
    }

    /**
     * Update a specific user.
     * This private endpoint is used to modify one user.
     * @param username 
     * @param requestBody 
     * @returns UserDTO User update successful.
     * @throws ApiError
     */
    public static async updateUser(
username: string,
requestBody?: UserModificationDTO,
): Promise<UserDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/users/${username}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Delete a specific user.
     * This private endpoint is used to remove one user.
     * @param username 
     * @returns ApiMessageDTO User deletion successful.
     * @throws ApiError
     */
    public static async deleteUser(
username: string,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'DELETE',
            path: `/users/${username}`,
        });
        return result.body;
    }

}