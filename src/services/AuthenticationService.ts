/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ApiMessageDTO } from '../models/ApiMessageDTO';
import type { LoginRequestDTO } from '../models/LoginRequestDTO';
import type { LoginSuccessDTO } from '../models/LoginSuccessDTO';
import type { RegisterDTO } from '../models/RegisterDTO';
import { request as __request } from '../core/request';

export class AuthenticationService {

    /**
     * Register a new user.
     * This public endpoint is used to register a new user.
     * @param requestBody 
     * @returns ApiMessageDTO Register successful.
     * @throws ApiError
     */
    public static async register(
requestBody?: RegisterDTO,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'POST',
            path: `/auth/register`,
            body: requestBody,
            errors: {
                400: `Username or Email Already taken.`,
            },
        });
        return result.body;
    }

    /**
     * Login
     * This public endpoint is used to login an existing user.
     * @param requestBody 
     * @returns LoginSuccessDTO Login successful.
     * @throws ApiError
     */
    public static async login(
requestBody?: LoginRequestDTO,
): Promise<LoginSuccessDTO> {
        const result = await __request({
            method: 'POST',
            path: `/auth/login`,
            body: requestBody,
            errors: {
                401: `Unauthorized`,
            },
        });
        return result.body;
    }

    /**
     * Logout.
     * This private endpoint is used to logout a logged user.
     * @returns ApiMessageDTO Logout successful.
     * @throws ApiError
     */
    public static async logout(): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'POST',
            path: `/auth/logout`,
            errors: {
                401: `Unauthorized`,
            },
        });
        return result.body;
    }

}