/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ApiMessageDTO } from '../models/ApiMessageDTO';
import type { LessonAddQuizDTO } from '../models/LessonAddQuizDTO';
import type { LessonAddSignDTO } from '../models/LessonAddSignDTO';
import type { LessonCreationDTO } from '../models/LessonCreationDTO';
import type { LessonDTO } from '../models/LessonDTO';
import type { LessonModificationDTO } from '../models/LessonModificationDTO';
import { request as __request } from '../core/request';

export class LessonService {

    /**
     * Add sign to lesson.
     * This private endpoint is used to add a sign to the lesson in the database.
     * @param requestBody 
     * @returns ApiMessageDTO Added Sign successfully.
     * @throws ApiError
     */
    public static async addSignToLesson(
requestBody?: LessonAddSignDTO,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/lessons/addSign/`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Add quiz to lesson.
     * This private endpoint is used to add a quiz to the lesson in the database.
     * @param requestBody 
     * @returns ApiMessageDTO Added Quiz successfully.
     * @throws ApiError
     */
    public static async addQuizToLesson(
requestBody?: LessonAddQuizDTO,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/lessons/addQuiz/`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Get the lesson.
     * This private endpoint is used to get the lesson with the given name in the database.
     * @param name 
     * @returns LessonDTO Get lesson successful.
     * @throws ApiError
     */
    public static async getLesson(
name: string,
): Promise<LessonDTO> {
        const result = await __request({
            method: 'GET',
            path: `/lessons/${name}`,
        });
        return result.body;
    }

    /**
     * Update the lesson.
     * This private endpoint is used to update a lesson in the database.
     * @param name 
     * @param requestBody 
     * @returns LessonDTO Lesson updated successfully.
     * @throws ApiError
     */
    public static async updateLesson(
name: string,
requestBody?: LessonModificationDTO,
): Promise<LessonDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/lessons/${name}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Delete the lesson.
     * This private endpoint is used to delete a lesson in the database.
     * @param name 
     * @returns ApiMessageDTO Lesson deleted successfully.
     * @throws ApiError
     */
    public static async deleteLesson(
name: string,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'DELETE',
            path: `/lessons/${name}`,
        });
        return result.body;
    }

    /**
     * Get all lessons.
     * This private endpoint is used to get all the lessons of a user in the database.
     * @returns LessonDTO Get lessons successful.
     * @throws ApiError
     */
    public static async getLessons(): Promise<Array<LessonDTO>> {
        const result = await __request({
            method: 'GET',
            path: `/lessons`,
        });
        return result.body;
    }

    /**
     * Add the lesson.
     * This private endpoint is used to add a new lesson in the database.
     * @param requestBody 
     * @returns LessonDTO Lesson created successfully.
     * @throws ApiError
     */
    public static async createLesson(
requestBody?: LessonCreationDTO,
): Promise<LessonDTO> {
        const result = await __request({
            method: 'POST',
            path: `/lessons`,
            body: requestBody,
        });
        return result.body;
    }

}