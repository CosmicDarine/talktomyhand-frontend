/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ApiMessageDTO } from '../models/ApiMessageDTO';
import type { PollCreationDTO } from '../models/PollCreationDTO';
import type { PollDTO } from '../models/PollDTO';
import type { PollModificationDTO } from '../models/PollModificationDTO';
import { request as __request } from '../core/request';

export class PollService {

    /**
     * Get poll by id
     * This private endpoint is used to get the poll with the given id in the database.
     * @param id 
     * @returns PollDTO Got poll successfully.
     * @throws ApiError
     */
    public static async getPollById(
id: number,
): Promise<PollDTO> {
        const result = await __request({
            method: 'GET',
            path: `/polls/${id}`,
        });
        return result.body;
    }

    /**
     * Get all polls.
     * This private endpoint is used to get all polls created.
     * @returns PollDTO Get polls successful.
     * @throws ApiError
     */
    public static async getPolls(): Promise<Array<PollDTO>> {
        const result = await __request({
            method: 'GET',
            path: `/admin/polls`,
        });
        return result.body;
    }

    /**
     * Add the poll.
     * This private endpoint is used to add a new poll in the database.
     * @param requestBody 
     * @returns PollDTO Poll creation successful.
     * @throws ApiError
     */
    public static async createPoll(
requestBody?: PollCreationDTO,
): Promise<PollDTO> {
        const result = await __request({
            method: 'POST',
            path: `/admin/polls`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Modifiy the poll.
     * This private endpoint is used to modifiy a poll in the database.
     * @param requestBody 
     * @returns PollDTO Poll modification successful.
     * @throws ApiError
     */
    public static async updatePoll(
requestBody?: PollModificationDTO,
): Promise<PollDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/admin/polls`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Delete the poll.
     * This private endpoint is used to delete a poll in the database.
     * @param id 
     * @returns ApiMessageDTO Poll deleted successfully.
     * @throws ApiError
     */
    public static async deletePollById(
id: number,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'DELETE',
            path: `/admin/polls/${id}`,
        });
        return result.body;
    }

}