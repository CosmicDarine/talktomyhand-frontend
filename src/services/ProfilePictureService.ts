/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ApiMessageDTO } from '../models/ApiMessageDTO';
import type { ProfilePictureCreationDTO } from '../models/ProfilePictureCreationDTO';
import type { ProfilePictureDTO } from '../models/ProfilePictureDTO';
import { request as __request } from '../core/request';

export class ProfilePictureService {

    /**
     * Get a specific profile's picture
     * This private endpoint is used to get a profile picture.
     * @param username 
     * @returns ProfilePictureDTO Get file successful.
     * @throws ApiError
     */
    public static async getProfilePicture(
username: string,
): Promise<ProfilePictureDTO> {
        const result = await __request({
            method: 'GET',
            path: `/files/picture/${username}`,
        });
        return result.body;
    }

    /**
     * Change a specific user's profile picture.
     * This private endpoint is used to upload a profile picture.
     * @param requestBody 
     * @returns ApiMessageDTO Upload successful.
     * @throws ApiError
     */
    public static async uploadProfilePicture(
requestBody?: ProfilePictureCreationDTO,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'POST',
            path: `/files/picture`,
            body: requestBody,
        });
        return result.body;
    }

}