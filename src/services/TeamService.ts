/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ApiMessageDTO } from '../models/ApiMessageDTO';
import type { TeamCreationDTO } from '../models/TeamCreationDTO';
import type { TeamDTO } from '../models/TeamDTO';
import type { TeamNameModificationDTO } from '../models/TeamNameModificationDTO';
import type { TeamSubscriptionDTO } from '../models/TeamSubscriptionDTO';
import { request as __request } from '../core/request';

export class TeamService {

    /**
     * Get all teams.
     * This private endpoint is used to get all the teams created.
     * @returns TeamDTO Get teams successful.
     * @throws ApiError
     */
    public static async getTeams(): Promise<Array<TeamDTO>> {
        const result = await __request({
            method: 'GET',
            path: `/teams`,
        });
        return result.body;
    }

    /**
     * Add the team.
     * This private endpoint is used to add a new team in the database.
     * @param requestBody 
     * @returns ApiMessageDTO Team creation successful.
     * @throws ApiError
     */
    public static async createTeam(
requestBody?: TeamCreationDTO,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'POST',
            path: `/teams`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Modify the team's name
     * This private endpoint is used to update the name of a team in the database.
     * @param requestBody 
     * @returns ApiMessageDTO Team updated successfully.
     * @throws ApiError
     */
    public static async updateTeamName(
requestBody?: TeamNameModificationDTO,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/teams`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Get the team.
     * This private endpoint is used to get the team specified by its name.
     * @param name 
     * @returns TeamDTO Get teams successful.
     * @throws ApiError
     */
    public static async getTeam(
name: string,
): Promise<TeamDTO> {
        const result = await __request({
            method: 'GET',
            path: `/teams/${name}`,
        });
        return result.body;
    }

    /**
     * Delete a specific team.
     * This private endpoint is used to remove one team.
     * @param name 
     * @returns ApiMessageDTO Team deleted successfully.
     * @throws ApiError
     */
    public static async deleteTeamByName(
name: string,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'DELETE',
            path: `/teams/${name}`,
        });
        return result.body;
    }

    /**
     * Subscribe to a team
     * This private endpoint is used to subscribe to a team.
     * @param requestBody 
     * @returns ApiMessageDTO Subscribed to team successfully
     * @throws ApiError
     */
    public static async subscribeToTeam(
requestBody?: TeamSubscriptionDTO,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/team/subscription`,
            body: requestBody,
        });
        return result.body;
    }

}