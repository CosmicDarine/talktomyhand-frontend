/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ApiMessageDTO } from '../models/ApiMessageDTO';
import type { SignCreationDTO } from '../models/SignCreationDTO';
import type { SignDTO } from '../models/SignDTO';
import type { SignModificationDTO } from '../models/SignModificationDTO';
import { request as __request } from '../core/request';

export class DialectService {

    /**
     * Search sign.
     * This private endpoint is used to get all the sign that are linked to the keyword in the data base.
     * @param keyword 
     * @returns SignDTO Search sign successfully.
     * @throws ApiError
     */
    public static async searchSign(
keyword: string,
): Promise<Array<SignDTO>> {
        const result = await __request({
            method: 'GET',
            path: `/signs/search/${keyword}`,
        });
        return result.body;
    }

    /**
     * Get sign by traduction
     * This private endpoint is used to get the sign with the given name in the database.
     * @param traduction 
     * @returns SignDTO Got sign successfully.
     * @throws ApiError
     */
    public static async getSignByTraduction(
traduction: string,
): Promise<SignDTO> {
        const result = await __request({
            method: 'GET',
            path: `/signs/${traduction}`,
        });
        return result.body;
    }

    /**
     * Delete a specific sign.
     * This private endpoint is used to remove one sign.
     * @param traduction 
     * @returns ApiMessageDTO Sign deletion successful.
     * @throws ApiError
     */
    public static async deleteSignByTraduction(
traduction: string,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'DELETE',
            path: `/signs/${traduction}`,
        });
        return result.body;
    }

    /**
     * Get all signs
     * This private endpoint is used to get all the sign in the database.
     * @returns SignDTO Get sign successful.
     * @throws ApiError
     */
    public static async getSigns(): Promise<Array<SignDTO>> {
        const result = await __request({
            method: 'GET',
            path: `/signs`,
        });
        return result.body;
    }

    /**
     * Add the sign.
     * This private endpoint is used to add a new sign in the database.
     * @param requestBody 
     * @returns SignDTO Sign creation successful.
     * @throws ApiError
     */
    public static async createSign(
requestBody?: SignCreationDTO,
): Promise<SignDTO> {
        const result = await __request({
            method: 'POST',
            path: `/admin/signs`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Modifiy the sign.
     * This private endpoint is used to modifiy a sign in the database.
     * @param requestBody 
     * @returns SignDTO Sign modification successful.
     * @throws ApiError
     */
    public static async updateSign(
requestBody?: SignModificationDTO,
): Promise<SignDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/admin/signs`,
            body: requestBody,
        });
        return result.body;
    }

}