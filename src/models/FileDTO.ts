/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type FileDTO = {
    id?: number;
    video?: string;
    gltf?: string;
    name?: string;
}