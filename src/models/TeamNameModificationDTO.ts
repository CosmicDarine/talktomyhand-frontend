/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TeamNameModificationDTO = {
    name?: string;
    newName?: string;
}