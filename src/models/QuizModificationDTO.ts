/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PollDTO } from './PollDTO';

export type QuizModificationDTO = {
    id?: number;
    name?: string;
    polls?: Array<PollDTO>;
}