/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PollDTO } from './PollDTO';

export type QuizCreationDTO = {
    name?: string;
    polls?: Array<PollDTO>;
}