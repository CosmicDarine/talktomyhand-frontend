/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TeamCreationDTO = {
    name?: string;
    points?: number;
    users?: Array<string>;
}