/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UserModificationDTO = {
    idUser?: number;
    username?: string;
    firstname?: string;
    lastname?: string;
    birthdate?: string;
    sex?: string;
    email?: string;
    points?: number;
    level?: number;
    team?: string;
    password?: string;
}