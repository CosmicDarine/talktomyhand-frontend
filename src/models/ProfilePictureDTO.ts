/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ProfilePictureDTO = {
    id?: number;
    image?: string;
    username?: string;
}