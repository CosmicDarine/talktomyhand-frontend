/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type SignDTO = {
    id?: number;
    traduction?: string;
    description?: string;
    difficulty?: string;
}