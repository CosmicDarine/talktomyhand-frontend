/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type FileCreationDTO = {
    video?: string;
    gltf?: string;
    name?: string;
}