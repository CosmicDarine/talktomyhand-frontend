/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TeamSubscriptionDTO = {
    teamName?: string;
    user?: string;
}