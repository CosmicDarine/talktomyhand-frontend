/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { QuizDTO } from './QuizDTO';
import type { SignDTO } from './SignDTO';

export type LessonCreationDTO = {
    name?: string;
    difficulty?: number;
    points?: number;
    signs?: Array<SignDTO>;
    quiz?: QuizDTO;
}