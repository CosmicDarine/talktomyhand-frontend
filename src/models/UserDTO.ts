/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FileDTO } from './FileDTO';
import type { LessonDTO } from './LessonDTO';
import type { RoleDTO } from './RoleDTO';

export type UserDTO = {
    idUser?: number;
    username?: string;
    firstname?: string;
    lastname?: string;
    birthdate?: string;
    email?: string;
    password?: string;
    sex?: string;
    points?: number;
    level?: number;
    team?: string;
    completedLessons?: Array<LessonDTO>;
    roles?: Array<RoleDTO>;
    profilePicture?: FileDTO;
}