/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type QuizAddPollDTO = {
    name?: string;
    polls?: Array<number>;
}