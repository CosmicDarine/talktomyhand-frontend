/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PollCreationDTO = {
    question?: string;
    questionSign?: string;
    propositions?: string;
}