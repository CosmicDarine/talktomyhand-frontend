/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TeamModificationDTO = {
    id?: number;
    name?: string;
    newName?: string;
    points?: number;
    users?: Array<string>;
}