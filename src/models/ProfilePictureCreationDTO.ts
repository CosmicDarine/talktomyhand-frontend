/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ProfilePictureCreationDTO = {
    image?: string;
    username?: string;
}