/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PollDTO = {
    id?: number;
    question?: string;
    questionSign?: string;
    propositions?: string;
}