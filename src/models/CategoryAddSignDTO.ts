/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type CategoryAddSignDTO = {
    name?: string;
    signs?: Array<string>;
}