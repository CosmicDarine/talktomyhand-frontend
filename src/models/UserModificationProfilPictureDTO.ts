/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FileDTO } from './FileDTO';

export type UserModificationProfilPictureDTO = {
    username?: string;
    profilePicture?: FileDTO;
}