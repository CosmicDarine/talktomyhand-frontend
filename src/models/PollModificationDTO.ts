/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PollModificationDTO = {
    id?: number;
    question?: string;
    questionSign?: string;
    propositions?: string;
}