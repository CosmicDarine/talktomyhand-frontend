/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SignDTO } from './SignDTO';

export type CategoryDTO = {
    id?: number;
    name?: string;
    signs?: Array<SignDTO>;
}