/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { LessonDTO } from './LessonDTO';

export type UserSimpleDTO = {
    username?: string;
    firstname?: string;
    lastname?: string;
    birthdate?: string;
    sex?: string;
    points?: number;
    level?: number;
    team?: string;
    completedLessons?: Array<LessonDTO>;
    email?: string;
}