/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type LessonAddSignDTO = {
    name?: string;
    signs?: Array<string>;
}