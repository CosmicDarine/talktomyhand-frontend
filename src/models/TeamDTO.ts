/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UserDTO } from './UserDTO';

export type TeamDTO = {
    id?: number;
    name?: string;
    points?: number;
    users?: Array<UserDTO>;
}