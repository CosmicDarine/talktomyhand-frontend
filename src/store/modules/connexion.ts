
import States from "@/store/States";
import StateHelper from "@/store/StateHelper";
import { AuthenticationService } from "@/services/AuthenticationService";
import { OpenAPI } from "@/core/OpenAPI";
import router from "@/router/router";
import Notifications from '../../util/Notifications';
//import { defaults } from "chart.js";

function initialState() {
    return {
        login: {
            state: States.INIT,
            message: "string",
            isLoggedIn: false,
            data: {
                jwt: "string",
                username: "username",
                roles: [],
            }
        },
        register: {
            state: States.INIT,
            message: "string",
        }
    }
}

const connexion = {
    state: () => (initialState()),
    getters: {
        getLogin(state) {
            return state.login;
        },
        getRegister(state) {
            return state.register;
        },
        getJwt(state) {
            return state.login.data.jwt;
        },
        isLoggedIn(state) {
            return state.login.isLoggedIn;
        },
        isAdmin(state) {
            for (let i = 0; i < state.login.data.roles.length; i++) {
                const e = state.login.data.roles[i];
                if (e === 'ROLE_ADMIN') {
                    return true;
                }
            }
            return false;
        }
    },


    actions: {
        actionLogin({ commit }, loginRequestDTO) {
            // First commit the waiting mutation
            commit('mutationLoginWaiting', {});
            //const api = new AuthenticationService();
            AuthenticationService
                .login(loginRequestDTO)
                .then(loginSuccessDTO => {
                    commit('mutationLoginSuccess', { ...loginSuccessDTO });
                })
                .catch(err => {
                    StateHelper.simpleErrorManagement(err, 'mutationLoginError', commit);
                })
        },
        
       actionRegister({ commit }, registerDTO) {
           // First commit the waiting mutation
           commit('mutationRegisterWaiting', {});
           //const api = new AuthenticationService();
           AuthenticationService
           .register(registerDTO)
           .then(registerSuccessDTO => {
            commit('mutationRegisterSuccess', { ...registerSuccessDTO });
           })
           .catch(err => {
            StateHelper.simpleErrorManagement(err, 'mutationRegisterError', commit);
           })
       },
       
       actionLogout({ commit }) {
           // First commit the waiting mutation
           commit('mutationLogoutWaiting', {});
           //const api = new AuthenticationService();
           AuthenticationService
           .logout()
           .then(logoutSuccessDTO => {
               commit('mutationLogoutSuccess', {...logoutSuccessDTO});
           })
           .catch(err => {
               StateHelper.simpleErrorManagement(err, 'mutationLogoutError', commit);
           })
       },
   },
  
    mutations: {
       mutationLoginWaiting(state) {
           state.login.state = States.WAITING;
       },
       mutationLoginSuccess(state, data) {
           state.login.state = States.SUCCESS;
           state.login.data = { ...data };
           state.login.isLoggedIn = true;
           const jwt = data.jwt;

           OpenAPI.TOKEN = jwt;

           //localStorage.setItem('jwt', jwt);
           //OpenAPI.HEADERS!['Authorization'] = jwt;

           // Go to the wanted url or to the homepage
           if (router.currentRoute.params.nextUrl !== undefined) {
               router.push(router.currentRoute.params.nextUrl)
           } else {
               router.push('/');
           }
           Notifications.success("Authentication Réussie", "Bienvenue " + data.username + " !");
       },
       mutationLoginError(state, data) {
           state.login.state = States.ERROR;
           state.login.message = data.message;
           localStorage.removeItem('jwt');
           Notifications.error("Erreur d'Authentication", data.message);
       },
       mutationRegisterWaiting(state) {
           state.register.state = States.WAITING;
       },
       mutationRegisterSuccess(state, data) {
           state.register.state = States.SUCCESS;
           state.register.data = { ...data };

           // Reset the login for better UX
           state.login = initialState().login;

           Notifications.success("Inscription Réussie", "Vous pouvez vous connecter maintenant.");
           router.push({ name: 'login' });
       },
       mutationRegisterError(state, data) {
           state.register.state = States.ERROR;
           state.register.message = data.message;
           Notifications.error("Erreur d'Inscription", data.message);
       },

       mutationLogoutWaiting(state) {
           state.login.state = States.WAITING;
       },
       mutationLogoutSuccess(state) {
           state.login = initialState().login;
           Notifications.success("Bye bye ! (^-^)", "Vous vous êtes déconnecté.");
       },
       mutationLogoutError(state, data) {
           state.login.state = States.ERROR;
           state.login.message = data.message;
           Notifications.error("Erreur de Déconnexion", data.message);
       },
   }
}

export default connexion;
