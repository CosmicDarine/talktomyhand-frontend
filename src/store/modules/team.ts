import States from "@/store/States";
import StateHelper from "@/store/StateHelper";
import Notifications from '../../util/Notifications';
import Utils from '../../util/Utils';
import { TeamService } from "@/services/TeamService";
import { UserService } from "@/services/UserService";

const team = {
  state: () => ({
    team: {
      state: States.INIT,
      message: "string",
      data: {
        "id": 0,
        "name": "",
        "points": 0,
        "users": []
      }
    },
    teams: {
      state: States.INIT,
      message: "string",
      data: []
    }
  }),

  getters: {
    getTeam(state) {
      return state.team;
    },
    getTeams(state) {
      return state.teams;
    },
    getTeamState(state) {
      return state.team.state;
    }
  },

  actions: {
    actionGetTeams({ commit }) {
      commit('mutationGetTeamsWaiting');
      TeamService
        .getTeams()
        .then(teams => {
          teams.sort(Utils.compareTeams);
          commit('mutationGetTeamsSuccess', teams);
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationGetTeamsError', commit);
        })
    },
    actionGetTeam({ commit }, name) {
      // First commit the waiting mutation
      commit('mutationGetTeamWaiting', {});
      TeamService
        .getTeam(name)
        .then(name => {
          commit('mutationGetTeamSuccess', { ...name });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationGetTeamError', commit);
        })
    },

    actionJoinTeam({ commit, dispatch }, teamSubscriptionDTO) {
      // First commit the waiting mutation
      commit('mutationJoinTeamWaiting', {});
      TeamService
        .subscribeToTeam(teamSubscriptionDTO)
        .then(TeamSubscriptionDTO => {
          commit('mutationJoinTeamSuccess', { ...TeamSubscriptionDTO });
          dispatch("actionGetTeams");
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationJoinTeamError', commit);
        })
    },

    actionLeaveTeam({ commit, dispatch }, teamSubscriptionDTO) {
      // First commit the waiting mutation
      commit('mutationLeaveTeamWaiting', {});
      TeamService
        .subscribeToTeam(teamSubscriptionDTO)
        .then(TeamSubscriptionDTO => {
          commit('mutationLeaveTeamSuccess', { ...TeamSubscriptionDTO });
          dispatch("actionGetTeams");
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationLeaveTeamError', commit);
        })
    },

    actionDeleteTeam({ commit, dispatch }, name) {
      // First commit the waiting mutation
      commit('mutationDeleteTeamWaiting', {});
      TeamService
        .deleteTeamByName(name)
        .then(name => {
          commit('mutationDeleteTeamSuccess', { ...name });
          dispatch("actionGetTeams");
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationDeleteTeamError', commit);
        })
    },
    actionCreateTeam({ commit, dispatch }, teamCreationDto) {
      // First commit the waiting mutation
      commit('mutationCreateTeamWaiting', {});
      TeamService
        .createTeam(teamCreationDto)
        .then(teamCreationDto => {
          commit('mutationCreateTeamSuccess', { ...teamCreationDto });
          dispatch("actionGetTeams");
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationCreateTeamError', commit);
        })
    },

    actionUpdateTeamName({ commit }, teamModificationDTO) {
      // First commit the waiting mutation
      commit('mutationUpdateTeamNameWaiting', {});
      TeamService
        .updateTeamName(teamModificationDTO)
        .then(teamModificationDTO => {
          commit('mutationUpdateTeamNameSuccess', { ...teamModificationDTO });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationUpdateTeamNameError', commit);
        })
    },
  },

  mutations: {
    // Get Team
    mutationGetTeamWaiting(state) {
      state.team.state = States.WAITING;
    },
    mutationGetTeamSuccess(state, data) {
      state.team.state = States.SUCCESS;
      state.team.data = { ...data };
    },
    mutationGetTeamError(state, data) {
      state.team.state = States.ERROR;
      state.team.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Get Teams
    mutationGetTeamsWaiting(state) {
      state.teams.state = States.WAITING;
    },
    mutationGetTeamsSuccess(state, data) {
      state.teams.state = States.SUCCESS;
      state.teams.data = [...data];

    },
    mutationGetTeamsError(state, data) {
      state.teams.state = States.ERROR;
      state.teams.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Delete Team
    mutationDeleteTeamWaiting(state) {
      state.team.state = States.WAITING;
    },
    mutationDeleteTeamSuccess(state) {
      state.team.state = States.SUCCESS;
      Notifications.success("Équipe", "supprimée avec succès.");
    },
    mutationDeleteTeamError(state, data) {
      state.team.state = States.ERROR;
      state.team.message = data.message;
      Notifications.error("Suppression impossible", "Veuillez contacter un administrateur.");
    },

    // Join Team
    mutationJoinTeamWaiting(state) {
      state.team.state = States.WAITING;
    },
    mutationJoinTeamSuccess(state) {
      state.team.state = States.SUCCESS;
      Notifications.success("En route pour de nouvelles aventures...", "Vous avez rejoint une équipe !");
    },
    mutationJoinTeamError(state, data) {
      state.team.state = States.ERROR;
      state.team.message = data.message;
      Notifications.error("Subscription impossible", "Veuillez contacter un administrateur.");
    },

    // Leave Team
    mutationLeaveTeamWaiting(state) {
      state.team.state = States.WAITING;
    },
    mutationLeaveTeamSuccess(state) {
      state.team.state = States.SUCCESS;
      Notifications.success("Un voyageur solitaire ...", "Équipe quittée avec succès.");
    },
    mutationLeaveTeamError(state, data) {
      state.team.state = States.ERROR;
      state.team.message = data.message;
      Notifications.error("Impossible de quitter la team", "Veuillez contacter un administrateur.");
    },

    // Create Team
    mutationCreateTeamWaiting(state) {
      state.team.state = States.WAITING;
    },
    mutationCreateTeamSuccess(state, teamDTO) {
      state.team.state = States.SUCCESS;
      state.team.data = teamDTO;
      state.teams.data = [...state.teams.data, teamDTO];
      
      Notifications.success("Création réussie", "L'équipe a été créée.");
    },
    mutationCreateTeamError(state, data) {
      state.team.state = States.ERROR;
      state.team.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Update Team
    mutationUpdateTeamNameWaiting(state) {
      state.team.state = States.WAITING;
    },
    mutationUpdateTeamNameSuccess(state, data) {
      state.team.state = States.SUCCESS;
      state.team.data = { ...data };
      Notifications.success("Modification Réussie", "L'équipe a été mise à jour.");
    },
    mutationUpdateTeamNameError(state, data) {
      state.team.state = States.ERROR;
      state.team.message = data.message;
      Notifications.error("Erreur de Modification", data.message);
    }
  }
}
export default team;