import States from "@/store/States";
import StateHelper from "@/store/StateHelper";
import { PollService } from "@/services/PollService";
import Notifications from '../../util/Notifications';
import Utils from "@/util/Utils";

const poll = {
  state: () => ({
    poll: {
      state: States.INIT,
      message: "string",
      data: {
        "id": 0,
        "question": "",
        "questionSign": "",
        "propositions": ""
      }
    },
    polls: {
      state: States.INIT,
      message: "string",
      data: [
      ]
    }
  }),

  getters: {
    getPoll(state) {
      return state.poll;
    },
    getPolls(state) {
      return state.polls;
    },
    getPollState(state) {
      return state.poll.state;
    }
  },

  actions: {
    actionGetPolls({ commit }) {
      // First commit the waiting mutation
      commit('mutationGetPollsWaiting');
      PollService
        .getPolls()
        .then(polls => {
          polls.sort(Utils.comparePolls);
          commit('mutationGetPollsSuccess', polls);
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationGetPollsError', commit);
        })
    },

    actionGetPoll({ commit }, id) {
      // First commit the waiting mutation
      commit('mutationGetPollWaiting', {});
      PollService
        .getPollById(id)
        .then(id => {
          commit('mutationGetPollSuccess', { ...id });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationGetPollError', commit);
        })
    },

    actionCreatePoll({ commit }, pollCreationDto) {
      // First commit the waiting mutation
      commit('mutationCreatePollWaiting', {});
      PollService
        .createPoll(pollCreationDto)
        .then(pollCreationDto => {
          commit('mutationCreatePollSuccess', { ...pollCreationDto });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationCreatePollError', commit);
        })
    },

    actionUpdatePoll({ commit }, pollModificationDTO) {
      // First commit the waiting mutation
      commit('mutationUpdatePollWaiting', {});
      PollService
        .updatePoll(pollModificationDTO)
        .then(pollModificationDTO => {
          commit('mutationUpdatePollSuccess', { ...pollModificationDTO });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationUpdatePollError', commit);
        })
    }
  },

  mutations: {
    // Get Poll
    mutationGetPollWaiting(state) {
      state.poll.state = States.WAITING;
    },
    mutationGetPollSuccess(state, data) {
      state.poll.state = States.SUCCESS;
      state.poll.data = { ...data };

      Notifications.debug("Poll", "Loaded Successfully.");
    },
    mutationGetPollError(state, data) {
      state.poll.state = States.ERROR;
      state.poll.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Get Polls
    mutationGetPollsWaiting(state) {
      state.polls.state = States.WAITING;
    },
    mutationGetPollsSuccess(state, data) {
      state.polls.state = States.SUCCESS;
      state.polls.data = [ ...data ];

      Notifications.debug("Polls", "Loaded Successfully.");
    },
    mutationGetPollsError(state, data) {
      state.polls.state = States.ERROR;
      state.polls.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Create Poll
    mutationCreatePollWaiting(state) {
      state.poll.state = States.WAITING;
    },
    mutationCreatePollSuccess(state, pollDTO) {
      state.poll.state = States.SUCCESS;
      state.poll.data = pollDTO;
      state.polls.data = [...state.polls.data, pollDTO];

      Notifications.success("Création réussie", "Le poll a été créé.");
    },
    mutationCreatePollError(state, data) {
      state.poll.state = States.ERROR;
      state.poll.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Update Poll
    mutationUpdatePollWaiting(state) {
      state.poll.state = States.WAITING;
    },
    mutationUpdatePollSuccess(state, data) {
      state.poll.state = States.SUCCESS;
      state.poll.data = { ...data };
      Notifications.success("Modification Réussie", "Le poll a été mis à jour.");
    },
    mutationUpdatePollError(state, data) {
      state.poll.state = States.ERROR;
      state.poll.message = data.message;
      Notifications.error("Erreur de Modification", data.message);
    }
  }
}

export default poll;