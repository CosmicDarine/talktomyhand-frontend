import States from "@/store/States";
import StateHelper from "@/store/StateHelper";
import { QuizService } from "@/services/QuizService";
import Notifications from '../../util/Notifications';
import Utils from "@/util/Utils";

const quiz = { 
  state: () => ({
    quizzes: {
      state: States.INIT,
      message: "string",
      data: [
      ]
    },
    quiz: {
      state: States.INIT,
      message: "string",
      data: {
        "id": 0,
        "name": "",
        "polls" : []
      }
    }
  }),

  getters: {
    getQuiz(state) {
      return state.quiz;
    },
    getQuizzes(state) {
      return state.quizzes;
    },
    getQuizState(state) {
      return state.quiz.state;
    }
  },

  actions: {
    actionGetQuizzes({commit}) {
      // First commit the waiting mutation
      commit('mutationGetQuizzesWaiting');
      QuizService
        .getQuizzes()
        .then(quizzes => {
          quizzes.sort(Utils.compare);
          commit('mutationGetQuizzesSuccess', quizzes);
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationGetQuizzesError', commit);
        })
    },

    actionGetQuiz({commit}, name) {
      // First commit the waiting mutation
      commit('mutationGetQuizWaiting', {});
      QuizService
        .getQuiz(name)
        .then(name => {
          commit('mutationGetQuizSuccess', { ...name });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationGetQuizError', commit);
        })
    },

    actionCreateQuiz({commit}, quizCreationDto) {
      // First commit the waiting mutation
      commit('mutationCreateQuizWaiting', {});
      QuizService
        .createQuiz(quizCreationDto)
        .then(quizCreationDto => {
          commit('mutationCreateQuizSuccess', { ...quizCreationDto });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationCreateQuizError', commit);
        })
    },

    actionUpdateQuiz({ commit }, quizModificationDTO) {
      // First commit the waiting mutation
      commit('mutationUpdateQuizWaiting', {});
      QuizService
        .updateQuiz(quizModificationDTO)
        .then(quizModificationDTO => {
          commit('mutationUpdateQuizSuccess', { ...quizModificationDTO });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationUpdateQuizError', commit);
        })
    },

  },

  mutations: {
    // Get Quiz
    mutationGetQuizWaiting(state) {
      state.quiz.state = States.WAITING;
    },
    mutationGetQuizSuccess(state, data) {
      state.quiz.state = States.SUCCESS;
      state.quiz.data = { ...data };
    },
    mutationGetQuizError(state, data) {
      state.quiz.state = States.ERROR;
      state.quiz.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Get Quizzes
    mutationGetQuizzesWaiting(state) {
      state.quizzes.state = States.WAITING;
    },
    mutationGetQuizzesSuccess(state, data) {
      state.quizzes.state = States.SUCCESS;
      state.quizzes.data = [ ...data ];
    },
    mutationGetQuizzesError(state, data) {
      state.quizzes.state = States.ERROR;
      state.quizzes.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Create Quiz
    mutationCreateQuizWaiting(state) {
      state.quiz.state = States.WAITING;
    },
    mutationCreateQuizSuccess(state, quizDTO) {
      state.quiz.state = States.SUCCESS;
      state.quiz.data = quizDTO;
      state.quizzes.data = [...state.quizzes.data, quizDTO];
      
      Notifications.success("Création réussie", "Le quiz a été créé.");
    },
    mutationCreateQuizError(state, data) {
      state.quiz.state = States.ERROR;
      state.quiz.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Update Quiz
    mutationUpdateQuizWaiting(state ) {
      state.quiz.state = States.WAITING;
    },
    mutationUpdateQuizSuccess(state, data) {
      state.quiz.state = States.SUCCESS;
      state.quiz.data = { ...data };
      Notifications.success("Modification Réussie", "Le quiz a été mis à jour.");
    },
    mutationUpdateQuizError(state, data) {
      state.quiz.state = States.ERROR;
      state.quiz.message = data.message;
      Notifications.error("Erreur de Modification", data.message);
    }
  }
}

export default quiz;