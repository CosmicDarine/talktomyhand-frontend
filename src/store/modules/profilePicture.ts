import { ProfilePictureDTO } from "@/models/ProfilePictureDTO";
import { ProfilePictureService } from "@/services/ProfilePictureService";
import Notifications from "@/util/Notifications";
import StateHelper from "../StateHelper";
import States from "../States";

const profilePicture = {
  state: () => ({
    profilePicture: {
      state: States.INIT,
      message: "string",
      data: {
        "id": 0,
        "name": "",
        "data": "",
      }
    }
  }),

  getters: {
    getProfilePicture(state) {
      return state.profilePicture;
    },

    getProfilePictureState(state) {
      return state.profilePicture.state;
    }
  },

  actions: {
    actionGetProfilePicture({ commit }, username) {
      console.log("LOL");
      // First commit the waiting mutation
      commit('mutationGetProfilePictureWaiting', {});
      ProfilePictureService
        .getProfilePicture(username)
        .then(profilePictureDTO => {
          console.log("LOL1");
          commit('mutationGetProfilePictureSuccess', { ...profilePictureDTO });
        })
        .catch(err => {
          
          if(err.body && err.body.message === "You don't have a profile picture for the moment.") {
            console.log("LOL2");
            const pic: ProfilePictureDTO = 
            {
              image: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAG1BMVEXMzMwAAACysrJ/f39MTEwZGRlmZmaZmZkzMzOjeu9dAAAB40lEQVR42u3WSZLiQBBEUWWkcrj/ibsFtDwx4caiOky1+G9V5QbhhCbYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOC3i4hPsUt/Z2HdWzm0HmscvX1JjZsKRytSlXel3aTGTYW1rOr7gNY0wqRGYqGfu9fYtqiPV8czHY83HvFj1DCpcUvh0MV3DK7LnzqC06TObYV67f4sW981j+NmUuPmwjjf15/nUXXdpBf72CT29EI/99z+Epv0zb5eyNHKTCz0xnmmWylN+fGfSa8TtEnoCk8p9Pp58rS8DoxLr8/WoT16XqGn2yleR0rXSzGp2UR75BR6ekaYuWFSs4n2SCv0jupxNvTLhI+p2aRqj6RCb9f+PzlA+g3Sswv9jdfix3O1Sc8u/D5WE3RPulQkmp5daYV+rKr13NMD3KRuD43LL/RjtbxaTOr22DUwp9Dfdm2sgWr08DCpaI+u75P0Qompy/Xy20HjXCraQ98naYWmesaHR7w6mkntHtokr1DUN+Mfpa9zX49h1aR2D22SVihaejGXu6+0OVvR6XXp+yJ9/cgtcgu/z912ZRpgUonWN6ktsgu/z93GfCVjE5Ma9xZKjL9c6uUUAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/ZH+moEHPt3DitAAAAAElFTkSuQmCC" 
            };
            commit('mutationGetProfilePictureSuccess', pic);
          }
          else {
            // StateHelper.simpleErrorManagement(err, 'mutationGetProfilePictureError', commit);
            const pic: ProfilePictureDTO = 
            {
              image: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAG1BMVEXMzMwAAACysrJ/f39MTEwZGRlmZmaZmZkzMzOjeu9dAAAB40lEQVR42u3WSZLiQBBEUWWkcrj/ibsFtDwx4caiOky1+G9V5QbhhCbYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOC3i4hPsUt/Z2HdWzm0HmscvX1JjZsKRytSlXel3aTGTYW1rOr7gNY0wqRGYqGfu9fYtqiPV8czHY83HvFj1DCpcUvh0MV3DK7LnzqC06TObYV67f4sW981j+NmUuPmwjjf15/nUXXdpBf72CT29EI/99z+Epv0zb5eyNHKTCz0xnmmWylN+fGfSa8TtEnoCk8p9Pp58rS8DoxLr8/WoT16XqGn2yleR0rXSzGp2UR75BR6ekaYuWFSs4n2SCv0jupxNvTLhI+p2aRqj6RCb9f+PzlA+g3Sswv9jdfix3O1Sc8u/D5WE3RPulQkmp5daYV+rKr13NMD3KRuD43LL/RjtbxaTOr22DUwp9Dfdm2sgWr08DCpaI+u75P0Qompy/Xy20HjXCraQ98naYWmesaHR7w6mkntHtokr1DUN+Mfpa9zX49h1aR2D22SVihaejGXu6+0OVvR6XXp+yJ9/cgtcgu/z912ZRpgUonWN6ktsgu/z93GfCVjE5Ma9xZKjL9c6uUUAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/ZH+moEHPt3DitAAAAAElFTkSuQmCC" 
            };
            commit('mutationGetProfilePictureSuccess', pic);
          }
          
        })
    },

    actionCreateProfilePicture({ commit }, profilePictureCreationDTO) {
      // First commit the waiting mutation
      commit('mutationCreateFileWaiting', {});
      ProfilePictureService
        .uploadProfilePicture(profilePictureCreationDTO)
        .then(profilePictureCreationDTO => {
          commit('mutationCreateFileSuccess', { ...profilePictureCreationDTO });
          Notifications.success("Image enregistrée avec succès", "");
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationCreateFileError', commit);
        })
    },
  },

  mutations: {
    // Get ProfilePicture
    mutationGetProfilePictureWaiting(state) {
      state.profilePicture.state = States.WAITING;
    },
    mutationGetProfilePictureSuccess(state, data) {
      state.profilePicture.state = States.SUCCESS;
      state.profilePicture.data = { ...data };
    },
    mutationGetProfilePictureError(state, data) {
      state.profilePicture.state = States.ERROR;
      state.profilePicture.message = data.message;
      Notifications.error("Erreur de Chargement", "L'image n'a pas pu être récupérée.");
    },

    // Create ProfilePicture
    mutationCreateProfilePictureWaiting(state) {
      state.profilePicture.state = States.WAITING;
    },
    mutationCreateProfilePictureSuccess(state, profilePictureDTO) {
      state.profilePicture.state = States.SUCCESS;
      state.profilePicture.data = profilePictureDTO;
    },
    mutationCreateProfilePictureError(state, data) {
      state.profilePicture.state = States.ERROR;
      state.profilePicture.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },
  }
}
export default profilePicture;