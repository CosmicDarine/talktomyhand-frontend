import States from "@/store/States";
import StateHelper from "@/store/StateHelper";
import { CategoryService } from "@/services/CategoryService";
import Notifications from '../../util/Notifications';
import Utils from '../../util/Utils';


const category = {
  state: () => ({
    categories: {
      state: States.INIT,
      message: "string",
      data: []
    },
    category: {
      state: States.INIT,
      message: "string",
      data: {
        "id": 0,
        "name": "",
        "signs": []
      }
    }
  }),

  getters: {
    getCategory(state) {
      return state.category;
    },

    getCategories(state) {
      return state.categories;
    },
    getCategoryState(state) {
      return state.category.state;
    }
  },

  actions: {
    actionGetCategories({ commit }) {
      // First commit the waiting mutation
      commit('mutationGetCategoriesWaiting');
      CategoryService
        .getCategories()
        .then(categories => {
          categories.sort(Utils.compareCategories);
          commit('mutationGetCategoriesSuccess', categories );
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationGetCategoriesError', commit);
        })
    },

    actionGetCategory({ commit }, name) {
      // First commit the waiting mutation
      commit('mutationGetCategoryWaiting', {});
      CategoryService
        .getCategory(name)
        .then(name => {
          commit('mutationGetCategorySuccess', { ...name });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationGetCategoryError', commit);
        })
    },

    actionDeleteCategory({ commit, dispatch }, name) {
      // First commit the waiting mutation
      commit('mutationDeleteCategoryWaiting', {});
      CategoryService
        .deleteCategoryByName(name)
        .then(name => {
          commit('mutationDeleteCategorySuccess', { ...name });
          dispatch("actionGetCategories");
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationDeleteCategoryError', commit);
        })
    },

    actionCreateCategory({ commit }, categoryCreationDto) {
      // First commit the waiting mutation
      commit('mutationCreateCategoryWaiting', {});
      CategoryService
        .createCategory(categoryCreationDto)
        .then(categoryCreationDto => {
          commit('mutationCreateCategorySuccess', { ...categoryCreationDto });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationCreateCategoryError', commit);
        })
    },

    actionUpdateCategory({ commit, dispatch }, categoryModificationDTO) {
      // First commit the waiting mutation
      commit('mutationUpdateCategoryWaiting', {});
      CategoryService
        .updateCategory(categoryModificationDTO)
        .then(categoryModificationDTO => {
          commit('mutationUpdateCategorySuccess', { ...categoryModificationDTO });
          dispatch("actionGetCategories");
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationUpdateCategoryError', commit);
        })
    },

    actionAddSignToCategory({ commit, dispatch }, categoryAddSignDTO) {
      // First commit the waiting mutation
      commit('mutationAddSignToCategoryWaiting', {});
      CategoryService
        .addSignToCategory(categoryAddSignDTO)
        .then(categoryAddSignDTO => {
          commit('mutationAddSignToCategorySuccess', { ...categoryAddSignDTO });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationAddSignToCategoryError', commit);
        })
    },
  },

  mutations: {
    // Get Category
    mutationGetCategoryWaiting(state) {
      state.category.state = States.WAITING;
    },
    mutationGetCategorySuccess(state, data) {
      state.category.state = States.SUCCESS;
      state.category.data = { ...data };
    },
    mutationGetCategoryError(state, data) {
      state.category.state = States.ERROR;
      state.category.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Get Categories
    mutationGetCategoriesWaiting(state) {
      state.categories.state = States.WAITING;
    },
    mutationGetCategoriesSuccess(state, data) {
      state.categories.state = States.SUCCESS;
      state.categories.data = [ ...data ].sort(Utils.compareCategories);
    },
    mutationGetCategoriesError(state, data) {
      state.categories.state = States.ERROR;
      state.categories.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Delete Category
    mutationDeleteCategoryWaiting(state) {
      state.category.state = States.WAITING;
    },
    mutationDeleteCategorySuccess(state) {
      state.category.state = States.SUCCESS;
      Notifications.success("Catégorie", "supprimée avec succès.");
    },
    mutationDeleteCategoryError(state, data) {
      state.category.state = States.ERROR;
      state.category.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Create Category
    mutationCreateCategoryWaiting(state) {
      state.category.state = States.WAITING;
    },
    mutationCreateCategorySuccess(state, categoryDTO) {
      state.category.state = States.SUCCESS;
      state.category.data = categoryDTO;
      state.categories.data = [...state.categories.data, categoryDTO].sort(Utils.compareCategories);

      Notifications.success("Création réussie", "La catégorie a été créée.");
    },
    mutationCreateCategoryError(state, data) {
      state.category.state = States.ERROR;
      state.category.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Update Category
    mutationUpdateCategoryWaiting(state) {
      state.category.state = States.WAITING;
    },
    mutationUpdateCategorySuccess(state, categoryDTO) {
      state.category.state = States.SUCCESS;
      state.category.data = { ...categoryDTO };

      Notifications.success("Modification Réussie", "La catégorie a été mise à jour.");
    },
    mutationUpdateCategoryError(state, data) {
      state.category.state = States.ERROR;
      state.category.message = data.message;
      Notifications.error("Erreur de Modification", data.message);
    },

    // Add sign to Category
    mutationAddSignToCategoryWaiting(state) {
      state.category.state = States.WAITING;
    },
    mutationAddSignToCategorySuccess(state, categoryDTO) {
      state.category.state = States.SUCCESS;
      state.category.data = { ...categoryDTO };
      Notifications.success("Modification Réussie", "Le signe a bien été ajouté à la catégorie.");
    },
    mutationAddSignToCategoryError(state, data) {
      state.category.state = States.ERROR;
      state.category.message = data.message;
      Notifications.error("Erreur d'ajout", data.message);
    }
  }

}
export default category;