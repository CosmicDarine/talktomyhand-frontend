import States from "@/store/States";
import StateHelper from "@/store/StateHelper";
import { LessonService } from "@/services/LessonService";
import Notifications from '../../util/Notifications';
import Utils from '../../util/Utils';

const lesson = {
  state: () => ({
    lesson: {
      state: States.INIT,
      message: "string",
      data: {
        "id": 0,
        "name": "",
        "difficulty": "",
        "points": 0,
        "signs": [],
        "quiz": {},
      }
    },
    lessons: {
      state: States.INIT,
      message: "string",
      data: []
    }
  }),

  getters: {
    getLesson(state) {
      return state.lesson;
    },
    getLessons(state) {
      return state.lessons;
    },
    getLessonState(state) {
      return state.lesson.state;
    }
  },

  actions: {
    actionGetLessons({ commit }) {
      // First commit the waiting mutation
      commit('mutationGetLessonsWaiting');
      LessonService
        .getLessons()
        .then(lessons => {
          lessons.sort(Utils.compareLessons);
          commit('mutationGetLessonsSuccess', lessons);
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationGetLessonsError', commit);
        })
    },

    actionGetLesson({ commit }, name) {
      // First commit the waiting mutation
      commit('mutationGetLessonWaiting', {});
      LessonService
        .getLesson(name)
        .then(name => {
          commit('mutationGetLessonSuccess', { ...name });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationGetLessonError', commit);
        })
    },

    actionDeleteLesson({ commit, dispatch }, name) {
      // First commit the waiting mutation
      commit('mutationDeleteLessonWaiting', {});
      LessonService
        .deleteLesson(name)
        .then(name => {
          commit('mutationDeleteLessonSuccess', { ...name });
          dispatch("actionGetLessons");
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationDeleteLessonError', commit);
        })
    },

    actionCreateLesson({ commit }, lessonCreationDto) {
      // First commit the waiting mutation
      commit('mutationCreateLessonWaiting', {});
      LessonService
        .createLesson(lessonCreationDto)
        .then(lessonDTO => {
          commit('mutationCreateLessonSuccess', { ...lessonDTO });
          Notifications.success("Leçon créée avec succès", "Nom leçon : " + lessonDTO.name);
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationCreateLessonError', commit);
        })
    },

    actionUpdateLesson({ commit }, lessonModificationDTO) {
      // First commit the waiting mutation
      commit('mutationUpdateLessonWaiting', {});
      LessonService
        .updateLesson(lessonModificationDTO)
        .then(lessonModificationDTO => {
          commit('mutationUpdateLessonSuccess', { ...lessonModificationDTO });
          Notifications.success("Leçon modifiée avec succès", "Nom leçon : " + lessonModificationDTO.name);
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationUpdateLessonError', commit);
        })
    },
  },

  mutations: {
    // Get Lesson
    mutationGetLessonWaiting(state) {
      state.lesson.state = States.WAITING;
    },
    mutationGetLessonSuccess(state, data) {
      state.lesson.data = { ...data };
      state.lesson.state = States.SUCCESS;
    },
    mutationGetLessonError(state, data) {
      state.lesson.state = States.ERROR;
      state.lesson.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Get Lessons
    mutationGetLessonsWaiting(state) {
      state.lessons.state = States.WAITING;
    },
    mutationGetLessonsSuccess(state, data) {
      state.lessons.state = States.SUCCESS;
      state.lessons.data = [...data];
    },
    mutationGetLessonsError(state, data) {
      state.lessons.state = States.ERROR;
      state.lessons.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Delete Lesson
    mutationDeleteLessonWaiting(state) {
      state.lesson.state = States.WAITING;
    },
    mutationDeleteLessonSuccess(state) {
      state.lesson.state = States.SUCCESS;
      Notifications.success("Leçon", "supprimée avec succès.");
    },
    mutationDeleteLessonError(state, data) {
      state.lesson.state = States.ERROR;
      state.lesson.message = data.message;
      Notifications.error("Suppression impossible", "Une erreur est survenue.");
    },

    // Create Lesson
    mutationCreateLessonWaiting(state) {
      state.lesson.state = States.WAITING;
    },
    mutationCreateLessonSuccess(state, lessonDTO) {
      state.lesson.state = States.SUCCESS;
      state.lesson.data = lessonDTO;
      state.lessons.data = [...state.lessons.data, lessonDTO];
    },
    mutationCreateLessonError(state, data) {
      state.lesson.state = States.ERROR;
      state.lesson.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Update Lesson
    mutationUpdateLessonWaiting(state) {
      state.lesson.state = States.WAITING;
    },
    mutationUpdateLessonSuccess(state, data) {
      state.lesson.state = States.SUCCESS;
      state.lesson.data = { ...data };
      Notifications.success("Modification Réussie", "La leçon a été mise à jour.");
    },
    mutationUpdateLessonError(state, data) {
      state.lesson.state = States.ERROR;
      state.lesson.message = data.message;
      Notifications.error("Erreur de Modification", data.message);
    }
  }
}

export default lesson;