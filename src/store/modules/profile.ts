import States from "@/store/States";
import StateHelper from "@/store/StateHelper";
import { UserService } from "@/services/UserService";
import Notifications from '../../util/Notifications';
import Utils from "@/util/Utils";

const user = {
  state: () => ({
    user: {
      state: States.INIT,
      message: "string",
      data: {
        "idUser": 0,
        "username": "",
        "firstname": "",
        "lastname": "",
        "birthdate": "2021-01-09",
        "email": "",
        "password": "",
        "sex": "",
        "completedLessons": [],
        "roles": [
          {
            "id": 1,
            "name": "USER_ROLE"
          }
        ],
        "image": {
          id: 0,
          image: "",
        },
        "points": 0,
        "level": 0
      }
    },
    users: {
      state: States.INIT,
      message: "string",
      data: []
    }
  }),
  getters: {
    getUser(state) {
      return state.user;
    },
    getUsers(state) {
      return state.users;
    },
    getUserState(state) {
      return state.user.state;
    },
    getUserImage(state) {
      const defaultImage = "data:application/octet-stream;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAG1BMVEXMzMwAAACysrJ/f39MTEwZGRlmZmaZmZkzMzOjeu9dAAAB40lEQVR42u3WSZLiQBBEUWWkcrj/ibsFtDwx4caiOky1+G9V5QbhhCbYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOC3i4hPsUt/Z2HdWzm0HmscvX1JjZsKRytSlXel3aTGTYW1rOr7gNY0wqRGYqGfu9fYtqiPV8czHY83HvFj1DCpcUvh0MV3DK7LnzqC06TObYV67f4sW981j+NmUuPmwjjf15/nUXXdpBf72CT29EI/99z+Epv0zb5eyNHKTCz0xnmmWylN+fGfSa8TtEnoCk8p9Pp58rS8DoxLr8/WoT16XqGn2yleR0rXSzGp2UR75BR6ekaYuWFSs4n2SCv0jupxNvTLhI+p2aRqj6RCb9f+PzlA+g3Sswv9jdfix3O1Sc8u/D5WE3RPulQkmp5daYV+rKr13NMD3KRuD43LL/RjtbxaTOr22DUwp9Dfdm2sgWr08DCpaI+u75P0Qompy/Xy20HjXCraQ98naYWmesaHR7w6mkntHtokr1DUN+Mfpa9zX49h1aR2D22SVihaejGXu6+0OVvR6XXp+yJ9/cgtcgu/z912ZRpgUonWN6ktsgu/z93GfCVjE5Ma9xZKjL9c6uUUAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/ZH+moEHPt3DitAAAAAElFTkSuQmCC";

      const makeBlob = function (dataUrl) {
        if (dataUrl === null || dataUrl === undefined || dataUrl === "" || dataUrl === "EMPTY") {
          dataUrl = defaultImage;
        }

        const arr = dataUrl.split(","),
          mime = arr[0].match(/:(.*?);/)[1],
          bstr = atob(arr[1]);
        let n = bstr.length;
        const u8arr = new Uint8Array(n);
        while (n--) {
          u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], { type: mime });
      };

      let blob;

      try {
        blob = makeBlob(state.user.data.image.image)
      } catch (error) {
        blob = makeBlob(defaultImage);
      }

      return URL.createObjectURL(blob);
    }

  },

  actions: {
    actionGetUser({ commit }, username) {
      // First commit the waiting mutation
      commit('mutationGetUserWaiting', {});
      UserService
        .getUserByUsername(username)
        .then(username => {
          commit('mutationGetUserSuccess', { ...username });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationGetUserError', commit);
        })
    },

    actionDeleteUser({ commit }, username) {
      // First commit the waiting mutation
      commit('mutationDeleteUserWaiting', {});
      UserService
        .deleteUser(username)
        .then(msg => {
          commit('mutationDeleteUserSuccess', { ...msg });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationDeleteUserError', commit);
        })
    },

    actionGetUsers({ commit }) {
      // First commit the waiting mutation
      commit('mutationGetUsersWaiting', {});
      UserService
        .getUsers()
        .then(users => {
          users.sort(Utils.compareUsers);
          commit('mutationGetUsersSuccess', { users: users });
        })
        .catch(err => {

          StateHelper.simpleErrorManagement(err, 'mutationGetUsersError', commit);
        })
    },

    actionGetMinimalUsers({ commit }) {
      // First commit the waiting mutation
      commit('mutationGetMinimalUsersWaiting', {});
      UserService
        .getUsersMinimal()
        .then(minimalUsers => {
          commit('mutationGetMinimalUsersSuccess', { minimalUsers: minimalUsers });
        })
        .catch(err => {

          StateHelper.simpleErrorManagement(err, 'mutationGetMinimalUsersError', commit);
        })
    },

    actionGetUsersByPoints({ commit }) {
      // First commit the waiting mutation
      commit('mutationGetUsersWaiting', {});
      UserService
        .getUsers()
        .then(users => {
          users.sort(Utils.compareUsersByPoints);
          commit('mutationGetUsersSuccess', { users: users });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationGetUsersError', commit);
        })
    },

    actionUpdateUser({ commit }, userModificationDTO) {
      // First commit the waiting mutation
      commit('mutationUpdateUserWaiting', {});
      UserService
        .updateUser(userModificationDTO.username, userModificationDTO)
        .then(userModificationDTO => {
          commit('mutationUpdateUserSuccess', { ...userModificationDTO });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationUpdateUserError', commit);
        })
    },

    actionUpdateUserPoints({ commit }, userPointsModificationDTO) {
      // First commit the waiting mutation
      commit('mutationUpdateUserPointsWaiting', {});
      UserService
        .updateUserPoints(userPointsModificationDTO)
        .then(userPointsModificationDTO => {
          commit('mutationUpdateUserPointsSuccess', { ...userPointsModificationDTO });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationUpdateUserPointsError', commit);
        })
    },

    actionUpdateUserByAdmin({ commit }, userDTO) {
      // First commit the waiting mutation
      commit('mutationUpdateUserWaiting', {});
      UserService
        .updateUserByAdmin(userDTO.username, userDTO)
        .then(userDTO => {
          commit('mutationUpdateUserSuccess', { ...userDTO });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationUpdateUserError', commit);
        })
    },
  },

  mutations: {
    // Get User
    mutationGetUserWaiting(state) {
      state.user.state = States.WAITING;
    },
    mutationGetUserSuccess(state, data) {
      state.user.state = States.SUCCESS;
      state.user.data = { ...data };
    },
    mutationGetUserError(state, data) {
      state.user.state = States.ERROR;
      state.user.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Delete User
    mutationDeleteUserWaiting(state) {
      state.user.state = States.WAITING;
    },
    mutationDeleteUserSuccess(state, data) {
      state.user.state = States.SUCCESS;
      state.user.data = { ...data };
      Notifications.success("Suppression utilisateur", "correctement supprimé.");
    },
    mutationDeleteUserError(state, data) {
      state.user.state = States.ERROR;
      state.user.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Get Users
    mutationGetUsersWaiting(state) {
      state.users.state = States.WAITING;
    },
    mutationGetUsersSuccess(state, data) {
      state.users.state = States.SUCCESS;
      state.users.data = { ...data };
    },
    mutationGetUsersError(state, data) {
      state.users.state = States.ERROR;
      state.users.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Get Minimal Users
    mutationGetMinimalUsersWaiting(state) {
      state.minimalUsers.state = States.WAITING;
    },
    mutationGetMinimalUsersSuccess(state, data) {
      state.minimalUsers.state = States.SUCCESS;
      state.minimalUsers.data = { ...data };
    },
    mutationGetMinimalUsersError(state, data) {
      state.minimalUsers.state = States.ERROR;
      state.minimalUsers.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Update User
    mutationUpdateUserWaiting(state) {
      state.user.state = States.WAITING;
    },
    mutationUpdateUserSuccess(state, userDTO) {
      state.user.state = States.SUCCESS;
      state.user.data = userDTO;
      Notifications.success("Modification Réussie", "Le profil a été mis à jour.");
    },
    mutationUpdateUserError(state, data) {
      state.user.state = States.ERROR;
      state.user.message = data.message;
      Notifications.error("Erreur de Modification", data.message);
    },

    // Update User points
    mutationUpdateUserPointsWaiting(state) {
      state.user.state = States.WAITING;
    },
    mutationUpdateUserPointsSuccess(state, data) {
      state.user.state = States.SUCCESS;
      state.user.data = { ...data };
    },
    mutationUpdateUserPointsError(state, data) {
      state.user.state = States.ERROR;
      state.user.message = data.message;
      Notifications.error("Erreur de Modification", data.message);
    },

    // Get image
    mutationGetUserImageWaiting(state,) {
      state.user.state = States.WAITING;
    },
    mutationGetUserImageSuccess(state, data) {
      state.user.state = States.SUCCESS;
      state.user.data.image = { ...data };
      Notifications.debug("Image de profil", "Chargée correctement.");
    },
    mutationGetUserImageError(state, data) {
      state.user.state = States.ERROR;
      state.user.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Update image
    mutationUpdateUserImageWaiting(state,) {
      state.user.state = States.WAITING;
    },
    mutationUpdateUserImageSuccess(state, data) {
      state.user.state = States.SUCCESS;
      state.user.data.image = { ...data };
      Notifications.success("Modification Réussie", "Votre image de profil a été mise à jour.");
    },
    mutationUpdateUserImageError(state, data) {
      state.user.state = States.ERROR;
      state.user.message = data.message;
      Notifications.error("Erreur de Modification", data.message);
    }
  }
}

export default user;
