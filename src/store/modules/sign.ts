import States from "@/store/States";
import StateHelper from "@/store/StateHelper";
import { DialectService } from "@/services/DialectService";
import Notifications from '../../util/Notifications';
import Utils from '../../util/Utils';

const sign = {
  state: () => ({
    signs: {
      state: States.INIT,
      message: "string",
      data: [
      ]
    },
    sign: {
      state: States.INIT,
      message: "string",
      data: {
        "id": 0,
        "traduction": "",
        "description": "",
        "video": "",
        "difficulty": "",
      }
    }
  }),

  getters: {
    getSign(state) {
      return state.sign;
    },
    getSigns(state) {
      return state.signs;
    },

    getSignState(state) {
      return state.sign.state;
    }
  },

  actions: {
    actionGetSigns({commit}) {
      // First commit the waiting mutation
      commit('mutationGetSignsWaiting');
      DialectService
        .getSigns()
        .then(signs => {
          signs.sort(Utils.compare);
          commit('mutationGetSignsSuccess',  signs );
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationGetSignsError', commit);
        })
    },

    actionGetSign({commit}, traduction) {
      // First commit the waiting mutation
      commit('mutationGetSignWaiting', {});
      DialectService
        .getSignByTraduction(traduction)
        .then(traduction => {
          commit('mutationGetSignSuccess', { ...traduction });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationGetSignError', commit);
        })
    },

    actionDeleteSign({commit, dispatch}, traduction) {
      // First commit the waiting mutation
      commit('mutationDeleteSignWaiting', {});
      DialectService
        .deleteSignByTraduction(traduction)
        .then(traduction => {
          commit('mutationDeleteSignSuccess', { ...traduction });
          dispatch("actionGetSigns");
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationDeleteSignError', commit);
        })
    },

    actionCreateSign({commit}, signCreationDto) {
      // First commit the waiting mutation
      commit('mutationCreateSignWaiting', {});
      DialectService
        .createSign(signCreationDto)
        .then(signCreationDto => {
          commit('mutationCreateSignSuccess', { ...signCreationDto });
          Notifications.success(signCreationDto.traduction + " en création...", "Le chargement des fichiers peut prendre quelques secondes.");
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationCreateSignError', commit);
        })
    },

    actionUpdateSign({ commit }, signModificationDTO) {
      // First commit the waiting mutation
      commit('mutationUpdateSignWaiting', {});
      DialectService
        .updateSign(signModificationDTO)
        .then(signModificationDTO => {
          commit('mutationUpdateSignSuccess', { ...signModificationDTO });
          Notifications.success("Signe modifié avec succès", "Nom signe : " + signModificationDTO.traduction);
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationUpdateSignError', commit);
        })
    },
  },

  mutations: {
    // Get Sign
    mutationGetSignWaiting(state) {
      state.sign.state = States.WAITING;
    },
    mutationGetSignSuccess(state, data) {
      state.sign.state = States.SUCCESS;
      state.sign.data = { ...data };
    },
    mutationGetSignError(state, data) {
      state.sign.state = States.ERROR;
      state.sign.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Get Signs
    mutationGetSignsWaiting(state) {
      state.signs.state = States.WAITING;
    },
    mutationGetSignsSuccess(state, data) {
      state.signs.state = States.SUCCESS;
      state.signs.data = [ ...data ];
      
    },
    mutationGetSignsError(state, data) {
      state.signs.state = States.ERROR;
      state.signs.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Delete Sign
    mutationDeleteSignWaiting(state) {
      state.sign.state = States.WAITING;
    },
    mutationDeleteSignSuccess(state) {
      state.sign.state = States.SUCCESS;
      Notifications.success("Signe", "supprimé avec succès.");
    },
    mutationDeleteSignError(state, data) {
      state.sign.state = States.ERROR;
      state.sign.message = data.message;
      Notifications.error("Suppression impossible", "Le signe est contenu dans une leçon ou un poll. Supprimez ces deux entités avant de supprimer le signe.");
    },

    // Create Sign
    mutationCreateSignWaiting(state) {
      state.sign.state = States.WAITING;
    },
    mutationCreateSignSuccess(state, signDTO) {
      state.sign.state = States.SUCCESS;
      state.sign.data = signDTO;
      state.signs.data = [...state.signs.data, signDTO];
      //Notifications.success("Création réussie", "Le signe a été créé.");
    },
    mutationCreateSignError(state, data) {
      state.sign.state = States.ERROR;
      state.sign.message = data.message;
      Notifications.error("Le signe spécifié existe déjà.", data.message);
    },

    // Update Sign
    mutationUpdateSignWaiting(state) {
      state.sign.state = States.WAITING;
    },
    mutationUpdateSignSuccess(state, data) {
      state.sign.state = States.SUCCESS;
      state.sign.data = { ...data };
      Notifications.success("Modification Réussie", "Le signe a été mis à jour.");
    },
    mutationUpdateSignError(state, data) {
      state.sign.state = States.ERROR;
      state.sign.message = data.message;
      Notifications.error("Erreur de Modification", data.message);
    }
  }

}
export default sign;