import States from "@/store/States";
import StateHelper from "@/store/StateHelper";
import { FileService } from "@/services/FileService";
import Notifications from '../../util/Notifications';
import Utils from '../../util/Utils';

const file = {
  state: () => ({
    file: {
      state: States.INIT,
      message: "string",
      data: {
        "id": 0,
        "name": "",
        "data": "",
      }
    }
  }),

  getters: {
    getFile(state) {
      return state.file;
    },

    getFileState(state) {
      return state.file.state;
    }
  },

  actions: {
    actionGetFile({ commit }, signName) {
      // First commit the waiting mutation
      commit('mutationGetFileWaiting', {});
      FileService
        .getSignVideo(signName)
        .then(fileDTO => {
          commit('mutationGetFileSuccess', { ...fileDTO });
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationGetFileError', commit);
        })
    },

    actionCreateFile({ commit }, fileCreationDTO) {
      // First commit the waiting mutation
      commit('mutationCreateFileWaiting', {});
      FileService
        .uploadSignVideo(fileCreationDTO)
        .then(fileCreationDTO => {
          commit('mutationCreateFileSuccess', { ...fileCreationDTO });
          Notifications.success("Fichier enregistré avec succès", "");
        })
        .catch(err => {
          StateHelper.simpleErrorManagement(err, 'mutationCreateFileError', commit);
        })
    },
  },

  mutations: {
    // Get File
    mutationGetFileWaiting(state) {
      state.file.state = States.WAITING;
    },
    mutationGetFileSuccess(state, data) {
      state.file.state = States.SUCCESS;
      state.file.data = { ...data };
    },
    mutationGetFileError(state, data) {
      state.file.state = States.ERROR;
      state.file.message = data.message;
      Notifications.error("Erreur de Chargement", "Le fichier n'a pas pu être récupéré. Nom du signe demandé : " + data.data);
    },

    // Create File
    mutationCreateFileWaiting(state) {
      state.file.state = States.WAITING;
    },
    mutationCreateFileSuccess(state, signDTO) {
      state.file.state = States.SUCCESS;
      state.file.data = signDTO;
      //Notifications.success("Création réussie", "Le signe a été créé.");
    },
    mutationCreateFileError(state, data) {
      state.file.state = States.ERROR;
      state.file.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },
  }
}
export default file;