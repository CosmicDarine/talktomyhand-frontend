import Vue from 'vue'
import Vuex from 'vuex'
import connexion from './modules/connexion'
import user from './modules/profile'
import sign from './modules/sign'
import category from './modules/category'
import lesson from './modules/lesson'
import quiz from './modules/quiz'
import poll from './modules/poll'
import team from './modules/team'
import file from './modules/file'
import profilePicture from './modules/profilePicture'
import createPersistedState from "vuex-persistedstate";
import { OpenAPI } from "../core/OpenAPI";

Vue.use(Vuex)

const dataState = createPersistedState({
  key: 'talktomykey',
  paths: ['connexion.login'],
  rehydrated: store => {
    const jwt = store.getters.getJwt;
    if(jwt) {
      OpenAPI.TOKEN = jwt;
    }
  }
})

export default new Vuex.Store({
  modules: {
    connexion,
    user,
    sign,
    category,
    lesson,
    quiz,
    poll,
    team,
    file,
    profilePicture
  },
  plugins: [dataState],
})
