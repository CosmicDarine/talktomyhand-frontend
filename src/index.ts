/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export { ApiError } from './core/ApiError';
export { OpenAPI } from './core/OpenAPI';

export type { ApiMessageDTO } from './models/ApiMessageDTO';
export type { CategoryAddSignDTO } from './models/CategoryAddSignDTO';
export type { CategoryCreationDTO } from './models/CategoryCreationDTO';
export type { CategoryDTO } from './models/CategoryDTO';
export type { CategoryModificationDTO } from './models/CategoryModificationDTO';
export type { FileCreationDTO } from './models/FileCreationDTO';
export type { FileDTO } from './models/FileDTO';
export type { LessonAddQuizDTO } from './models/LessonAddQuizDTO';
export type { LessonAddSignDTO } from './models/LessonAddSignDTO';
export type { LessonCreationDTO } from './models/LessonCreationDTO';
export type { LessonDTO } from './models/LessonDTO';
export type { LessonModificationDTO } from './models/LessonModificationDTO';
export type { LoginRequestDTO } from './models/LoginRequestDTO';
export type { LoginSuccessDTO } from './models/LoginSuccessDTO';
export type { PollCreationDTO } from './models/PollCreationDTO';
export type { PollDTO } from './models/PollDTO';
export type { PollModificationDTO } from './models/PollModificationDTO';
export type { ProfilePictureCreationDTO } from './models/ProfilePictureCreationDTO';
export type { ProfilePictureDTO } from './models/ProfilePictureDTO';
export type { QuizAddPollDTO } from './models/QuizAddPollDTO';
export type { QuizCreationDTO } from './models/QuizCreationDTO';
export type { QuizDTO } from './models/QuizDTO';
export type { QuizModificationDTO } from './models/QuizModificationDTO';
export type { RegisterDTO } from './models/RegisterDTO';
export type { RoleDTO } from './models/RoleDTO';
export type { SignCreationDTO } from './models/SignCreationDTO';
export type { SignDTO } from './models/SignDTO';
export type { SignModificationDTO } from './models/SignModificationDTO';
export type { TeamCreationDTO } from './models/TeamCreationDTO';
export type { TeamDTO } from './models/TeamDTO';
export type { TeamModificationDTO } from './models/TeamModificationDTO';
export type { TeamNameModificationDTO } from './models/TeamNameModificationDTO';
export type { TeamSubscriptionDTO } from './models/TeamSubscriptionDTO';
export type { UserAddPointsForLessonDTO } from './models/UserAddPointsForLessonDTO';
export type { UserDTO } from './models/UserDTO';
export type { UserModificationDTO } from './models/UserModificationDTO';
export type { UserModificationProfilPictureDTO } from './models/UserModificationProfilPictureDTO';
export type { UserSimpleDTO } from './models/UserSimpleDTO';

export { AuthenticationService } from './services/AuthenticationService';
export { CategoryService } from './services/CategoryService';
export { DialectService } from './services/DialectService';
export { FileService } from './services/FileService';
export { LessonService } from './services/LessonService';
export { PollService } from './services/PollService';
export { ProfilePictureService } from './services/ProfilePictureService';
export { QuizService } from './services/QuizService';
export { TeamService } from './services/TeamService';
export { UserService } from './services/UserService';
