import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import About from "../components/pages/About.vue";
import Home from "../components/pages/Home.vue";
import Register from "../components/pages/connexion/Register.vue";
import Categories from "../components/pages/Categories.vue";
import Login from "../components/pages/connexion/Login.vue";
import Lesson from "../components/pages/lessons/Lesson.vue";
import Lessons from "../components/pages/lessons/Lessons.vue";
import Sign from "../components/uniqueComponents/Sign.vue";
import Profile from "../components/pages/Profile.vue";
import Dictionary from "../components/pages/Dictionary.vue";
import Modelisation2 from "../components/pages/Modelisation2.vue";
import SignModelisation from "../components/uniqueComponents/SignModelisation.vue";
import Timeline from "../components/pages/Timeline.vue";
import Administration from "../components/pages/Administration.vue";
import Teams from "../components/team/Teams.vue";
import Team from "../components/team/Team.vue";

import store from "../store/store";

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/about",
    name: "about",
    component: About
  },
  {
    path: '/sign',
    name: 'Sign',
    component: Sign,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: '/lesson',
    name: 'Lesson',
    component: Lesson,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: '/lessons',
    name: 'Lessons',
    component: Lessons,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/lesson/:name",
    name: "lesson",
    component: Lesson,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/register",
    name: "register",
    component: Register
  },
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/categories",
    name: "categories",
    component: Categories,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/profile",
    name: "profile",
    component: Profile,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/dictionary",
    name: "dictionary",
    component: Dictionary,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/sign/:traduction",
    name: "sign",
    component: Sign,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/modelisation2",
    name: "modelisation2",
    component: Modelisation2,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/timeline",
    name: "timeline",
    component: Timeline,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/administration",
    name: "administration",
    component: Administration,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/teams",
    name: "teams",
    component: Teams,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/team/:name",
    name: "team",
    component: Team,
    meta: {
      requiresAuth: true,
    }
  }
]

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL || "/",
  routes
})

/**
 * Redirections for auth and admin
 * Source:
 * https://router.vuejs.org/guide/advanced/navigation-guards.html#per-route-guard
 */
router.beforeEach((to, from, next) => {
  // Auth required not Public
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // Not logged in
    if (!store.getters.isLoggedIn) {
      next({
        name: 'login',
        params: { nextUrl: to.fullPath }
      });
    }
    // Logged in
    else {
      // Admin is required
      if (to.matched.some(record => record.meta.isAdmin)) {
        // isAdmin
        if (store.getters.isAdmin) {
          next();
        }
        //is Not Admin
        else {
          next({ name: 'NotFound' });
        }
      }
      // Admin is not required
      else {
        next();
      }
    }
  }
  // Public
  else {
    next();
  }
});

export default router
