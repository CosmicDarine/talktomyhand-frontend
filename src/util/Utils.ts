

export default {
  formatDate(value) {
    if (value) {
      const date = new Date(value);
      const dd = String(date.getDate()).padStart(2, "0");
      const mm = String(date.getMonth() + 1).padStart(2, "0");
      const yyyy = date.getFullYear();

      const newDate = dd + "." + mm + "." + yyyy;
      return newDate;
    }
  },

  compare(a, b) {
    if (a.traduction < b.traduction) {
      return -1;
    } else if (a.traduction > b.traduction) {
      return 1;
    } else {
      return 0;
    }
  },

  compareLessons(a,b) {
    if (a.name < b.name) {
      return -1;
    } else if (a.name > b.name) {
      return 1;
    } else {
      return 0;
    }
  },

  compareUsersByLevel(a, b) {
    if (a.level < b.level) {
      return -1;
    } else if (a.level > b.level) {
      return 1;
    } else {
      return 0;
    }
  },

  compareTeams(a, b) {
    if (a.name < b.name) {
      return -1;
    } else if (a.name > b.name) {
      return 1;
    } else {
      return 0;
    }
  },

  compareCategories(a, b) {
    if (a.name < b.name) {
      return -1;
    } else if (a.name > b.name) {
      return 1;
    } else {
      return 0;
    }
  },

  compareUsers(a, b) {
    if (a.username < b.username) {
      return -1;
    } else if (a.username > b.username) {
      return 1;
    } else {
      return 0;
    }
  },

  compareUsersByPoints(a, b) {
    if (a.points < b.points) {
      return -1;
    } else if (a.points > b.points) {
      return 1;
    } else {
      return 0;
    }
  },

  comparePolls(a, b) {
    if (a.questionSign < b.questionSign) {
      return -1;
    } else if (a.questionSign > b.questionSign) {
      return 1;
    } else {
      return 0;
    }
  },

  formatDateForBackend(value) {
    if (value) {
      const date = new Date(value);
      const dd = String(date.getDate()).padStart(2, "0");
      const mm = String(date.getMonth() + 1).padStart(2, "0");
      const yyyy = date.getFullYear();

      const newDate = yyyy + "-" + mm + "-" + dd;
      return newDate;
    }
  },
  createDatetime(date, time) {
    const t = time.split(":");
    const datetime = new Date(date);
    datetime.setHours(t[0]);
    datetime.setMinutes(t[1]);
    return datetime;
  },
  getTime(datetime) {
    const offset = datetime.getTimezoneOffset() / 60;

    let time = "";
    time += datetime.getHours() < 10 ? "0" + (datetime.getHours() - offset) : (datetime.getHours() - offset);
    time += ":";
    time += datetime.getMinutes() < 10 ? "0" + datetime.getMinutes() : datetime.getMinutes()
    return time;
  },
  now() {
    return new Date();
  },
  nowTime() {
    let time = "";
    const date = new Date();
    time += date.getHours();
    time += ":";
    time += date.getMinutes();
    return time;
  }
};