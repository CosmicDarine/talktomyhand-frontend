#!/usr/bin/env bash

#docker-machine start default
docker login --username=$DOCKER_USRNAME --password=$DOCKER_PSWD

echo "Update and push talktomyhand application"
cd docker/frontend/
docker build -t cosmicdarine/talktomyhand-front:latest .
docker push cosmicdarine/talktomyhand-front:latest

